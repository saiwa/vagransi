# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
	# this should be modified for each project before spin-up
	#
	project_name = "example"


	# load local configuration file if exists
	#
	begin
		lcfg = YAML::load_file('.vlocal.conf.yml')
	rescue Errno::ENOENT
		puts 'Failed to load vagrantfile yml environment settings, using defaults...'
		lcfg = {}
	end
	# routine for setting default hash values
	#
	def hash_default(hash, key, def_val)
		hash[key] = hash.key?(key) ? hash[key] : def_val
	end

	# set project_name if specified in local config
	if lcfg.key?('project_name') == true
		project_name = lcfg['project_name']
	end

	# set defaults for any data that was not set
	hash_default lcfg, 'network', {}
	hash_default lcfg['network'], 'is_private', true
	hash_default lcfg['network'], 'ip_address', '192.168.33.20'
	hash_default lcfg['network'], 'hostname', "examp.le"
	hash_default lcfg['network'], 'forward', {}
	hash_default lcfg['network']['forward'], 'from', 0
	hash_default lcfg['network']['forward'], 'to', 0
	hash_default lcfg['network'], 'hostnames', {}
	hash_default lcfg['network']['hostnames'], 'enabled', true
	hash_default lcfg['network']['hostnames'], 'name', project_name
	hash_default lcfg['network']['hostnames'], 'aliases', []
	hash_default lcfg, 'vbox', {}
	hash_default lcfg['vbox'], 'memory', 2048
	hash_default lcfg['vbox'], 'use_gui', false
	hash_default lcfg, 'ansible_vars', {}
	hash_default lcfg['ansible_vars'], 'project_name', project_name
	hash_default lcfg['ansible_vars'], 'hostname', lcfg['network']['hostname']
	hash_default lcfg['ansible_vars'], 'mysql_database', project_name
	hash_default lcfg['ansible_vars'], 'mysql_user', project_name
	hash_default lcfg['ansible_vars'], 'mysql_password', 'secret' 
	hash_default lcfg, 'timeout', 240

	# check for missing plugins and install as necessary
	#
	required_plugins = ['vagrant-hostmanager', 'vagrant-vbguest', 'vagrant-cachier', 'vagrant-winnfsd']
	plugin_installed = false
	required_plugins.each do |plugin|
		unless Vagrant.has_plugin?(plugin)
			system "vagrant plugin install #{plugin}"
			plugin_installed = true
		end
	end
	# if new plugins installed, restart vagrant process
	if plugin_installed == true
		exec "vagrant #{ARGV.join' '}"
	end


	# specify base vagrant box
	#
	config.vm.box = "ubuntu/bionic64"
	config.vm.define "v-#{project_name}"
	config.vm.boot_timeout = lcfg['timeout']

	# disable automatic box update checking
	#
	config.vm.box_check_update = false


	# create a private network, which allows host-only access to the machine
	# using a specific IP
	#
	config.vm.network 'private_network', ip: lcfg['network']['ip_address']
	if lcfg['network']['forward']['from'] != 0
		config.vm.network "forwarded_port", guest: lcfg['network']['forward']['to'], host: lcfg['network']['forward']['from']
	end
	config.vm.hostname = lcfg['network']['hostname']

	# hostname management plugin
	#
	if lcfg['network']['hostnames']['enabled'] == true
		config.hostmanager.enabled = true
		config.hostmanager.manage_host = true
		config.hostmanager.manage_guest = true
		config.hostmanager.ignore_private_ip = false
		config.hostmanager.aliases = lcfg['network']['hostnames']['aliases']
	end


	# share an additional folder to the guest vm. the first argument is
	# the path on the host to the actual folder. the second argument is
	# the path on the guest to mount the folder. and the optional third 
	# argument is a set of non-required options.
	#
	config.vm.synced_folder ".", "/vagrant", disabled: true
	config.vm.synced_folder "shared", "/var/www/html", create: true, type: "nfs",
		mount_options: %w{rw,async,fsc,nolock,vers=3,udp,rsize=32768,wsize=32768,hard,noatime,actimeo=2}
	config.vm.synced_folder "ansible", "/ansible"

	
	# provider-specific configuration
	#
	config.vm.provider "virtualbox" do |vb|
		vb.memory = lcfg['vbox']['memory']
		vb.gui = lcfg['vbox']['use_gui']
	end

	# configuring apt cacheing
	#
	config.cache.scope = :box

	# generate inventory for ansible
	#
	File.open('ansible/inventory', 'w') do |f|
		f.write "#{lcfg['network']['ip_address']} ansible_connection=local"
	end

	# generate nginx variable file
	#
	File.open('ansible/assets/nginx_variables_file', 'w') do |f|
		f.write "set $app_domain #{lcfg['network']['hostname']};"
	end

	# ansible preparation
	#
	config.vm.provision :ansible_local do |ansible|
		ansible.provisioning_path = "/ansible"
		ansible.playbook = "prep.yml"
		ansible.inventory_path = "inventory"
		ansible.limit = "all"
	end

	# ansible main provisioning
	#
	config.vm.provision :ansible_local do |ansible|
		ansible.provisioning_path = "/ansible"
		ansible.extra_vars = lcfg['ansible_vars']
		ansible.playbook = "main.yml"
		ansible.inventory_path = "inventory"
		ansible.limit = "all"
	end
end
